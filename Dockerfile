FROM ubuntu:16.04
MAINTAINER cyber-punk, http://bitbucket.org/cyber-punk

ADD ./ /var/www/web/
WORKDIR /var/www/web/

# install system packages
RUN apt-get update -y
RUN apt-get install -y --no-install-recommends \
        build-essential \
        libssl-dev \
        libpq-dev \
        python3-dev \
        python3-pip
RUN apt-get install -y --no-install-recommends \
        uwsgi \
        uwsgi-plugin-python3

# install virtual environment & python packages
RUN pip3 install --upgrade pip && \
    pip3 install --upgrade virtualenv && \
    virtualenv -p /usr/bin/python3.5 --clear ./env/ && \
    . env/bin/activate && \
    ./env/bin/pip install -r ./requirements/production.txt

# setup workdir's user & group
RUN chown -R www-data /var/www/web && chgrp -R www-data /var/www/web

# setup application log directory
RUN mkdir -p /var/log/web
RUN chown -R www-data /var/log/web && chgrp -R www-data /var/log/web

# setup web-server log directories
RUN mkdir -p /var/log/uwsgi/app
RUN chown -R www-data /var/log/uwsgi/app && chgrp -R www-data /var/log/uwsgi/app

# configure web-server
ADD ./uwsgi/apps-available/application.xml /etc/uwsgi/apps-enabled/application.xml

# export volumes
VOLUME ["/var/www/web", "/var/log/web/", "/var/log/uwsgi/"]

CMD ["uwsgi", "--xml", "/etc/uwsgi/apps-enabled/application.xml"]
