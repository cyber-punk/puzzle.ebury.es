# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from mock import patch

from tests.common import TestCase
from app.config import settings


class TestApplication(TestCase):

    def test_settings_is_testing(self):
        self.assertTrue(self.app.settings.TESTING)

    def test_config_is_testing(self):
        self.assertTrue(self.app.config['TESTING'])

    def test_lazy_settings_is_testing(self):
        self.assertTrue(settings.TESTING)

    def test_run_default_params(self):
        with patch('flask.Flask.run') as m_run:
            self.app.run()
            m_run.assert_called_once_with(self.app.settings.HOST,
                                          self.app.settings.PORT)

    def test_run_custom_params(self):
        options = {'x': 1, 'y': 2, 'z': 3}
        with patch('flask.Flask.run') as m_run:
            self.app.run(host='0.0.0.0', port=80, **options)
            m_run.assert_called_once_with('0.0.0.0', 80, **options)
