# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from decimal import Decimal
from mock import patch, Mock

from flask import url_for

from tests.common import TestCase


class TestApiTradeResource(TestCase):

    def setUp(self):
        self.external_api_response1 = Mock()
        self.external_api_response1.status_code = 200
        self.external_api_response1.json.return_value = {
            'base': 'EUR',
            'rates': {'USD': Decimal('1.426225')}
        }

        self.external_api_response2 = Mock()
        self.external_api_response2.status_code = 200
        self.external_api_response2.json.return_value = {
            'base': 'EUR',
            'rates': {
                'USD': Decimal('1.426225'),
                'GBP': Decimal('1.2561')
            }
        }

    def test_get_item(self):
        with patch('app.api.services.requests.get',
                   return_value=self.external_api_response1):

            response = self.client.get(
                url=url_for('api_v1__currency_rate_resource',
                            base_currency='EUR',
                            rate_currency='USD'))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'base_currency': 'EUR',
            'rate_currency': 'USD',
            'rate': Decimal('1.426225')
        })

    def test_get_item_unsupported_currency(self):
        response = self.client.get(
            url=url_for('api_v1__currency_rate_resource',
                        base_currency='EUR',
                        rate_currency='XXX'))

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['error'], 'Validation error')
        self.assertDictEqual(response.data['details'], {
            'rate_currency': ['Not a valid choice.']
        })

    def test_get_list(self):
        from app.config import settings

        with patch('app.api.services.requests.get',
                   return_value=self.external_api_response2):

            response = self.client.get(
                url=url_for('api_v1__currency_rate_resource',
                            base_currency='EUR'))

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['limit'], settings.MAX_PAGING_LIMIT)
        self.assertEqual(response.data['offset'], 0)
        self.assertEqual(response.data['total'], 2)
        self.assertEqual(response.data['count'], 2)
        self.assertEqual(len(response.data['items']), 2)

        self.assertDictEqual(response.data['items'][0], {
            'base_currency': 'EUR',
            'rate_currency': 'GBP',
            'rate': Decimal('1.256100')
        })

        self.assertDictEqual(response.data['items'][1], {
            'base_currency': 'EUR',
            'rate_currency': 'USD',
            'rate': Decimal('1.426225')
        })
