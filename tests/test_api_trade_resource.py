# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime, timezone, timedelta
from decimal import Decimal
from mock import patch

from flask import url_for

from tests.common import TestCase, utils


class TestApiTradeResource(TestCase):

    def setUp(self):
        from app.api.services import TradeStorage

        self.storage = TradeStorage(self.app)
        self.storage.delete_list({})

        self.t0 = datetime(year=1985, month=10, day=26,
                           hour=1, minute=20, second=0, microsecond=0,
                           tzinfo=timezone(offset=-timedelta(hours=7)))
        self.t1 = datetime(year=2015, month=10, day=21,
                           hour=4, minute=29, second=0, microsecond=0,
                           tzinfo=timezone(offset=-timedelta(hours=7)))
        self.t2 = datetime(year=2018, month=5, day=27,
                           hour=23, minute=13, second=5, microsecond=0,
                           tzinfo=timezone(offset=timedelta(hours=1)))

        self.storage.create_item(utils.Attrs({
            'id': 'TR0123456',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'GBP',
            'buy_amount': Decimal('713.11'),
            'rate': Decimal('1.426225'),
            'booked_on': self.t0
        }, primary_key=['id']))

        self.storage.create_item(utils.Attrs({
            'id': 'TR789ABCD',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'EUR',
            'buy_amount': Decimal('535.55'),
            'rate': Decimal('1.0711'),
            'booked_on': self.t1
        }, primary_key=['id']))

    def test_get_item(self):
        response = self.client.get(
            url=url_for('api_v1__trade_resource', id='TR0123456'))

        self.assertEqual(response.status_code, 200)
        self.assertDictEqual(response.data, {
            'id': 'TR0123456',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'GBP',
            'buy_amount': Decimal('713.11'),
            'rate': Decimal('1.426225'),
            'booked_on': self.t0
        })

    def test_get_item_does_not_exist(self):
        response = self.client.get(
            url=url_for('api_v1__trade_resource', id='TRXXXXXXX'))

        self.assertEqual(response.status_code, 404)
        self.assertEqual(response.data['error'], 'Resource does not exist')

    def test_get_list(self):
        from app.config import settings

        response = self.client.get(
            url=url_for('api_v1__trade_resource'))

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['limit'], settings.MAX_PAGING_LIMIT)
        self.assertEqual(response.data['offset'], 0)
        self.assertEqual(response.data['total'], 2)
        self.assertEqual(response.data['count'], 2)
        self.assertEqual(len(response.data['items']), 2)

        self.assertDictEqual(response.data['items'][0], {
            'id': 'TR789ABCD',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'EUR',
            'buy_amount': Decimal('535.55'),
            'rate': Decimal('1.0711'),
            'booked_on': self.t1
        })

        self.assertDictEqual(response.data['items'][1], {
            'id': 'TR0123456',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'GBP',
            'buy_amount': Decimal('713.11'),
            'rate': Decimal('1.426225'),
            'booked_on': self.t0
        })

    def test_get_list_paging(self):
        response = self.client.get(
            url=url_for('api_v1__trade_resource'),
            query_params={'limit': 1, 'offset': 1})

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['limit'], 1)
        self.assertEqual(response.data['offset'], 1)
        self.assertEqual(response.data['total'], 2)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(len(response.data['items']), 1)

        self.assertDictEqual(response.data['items'][0], {
            'id': 'TR0123456',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'GBP',
            'buy_amount': Decimal('713.11'),
            'rate': Decimal('1.426225'),
            'booked_on': self.t0
        })

    def test_get_filtering(self):
        response = self.client.get(
            url=url_for('api_v1__trade_resource'),
            query_params={'buy_currency': 'EUR'})

        self.assertEqual(response.status_code, 200)

        self.assertEqual(response.data['total'], 1)
        self.assertEqual(response.data['count'], 1)
        self.assertEqual(len(response.data['items']), 1)

        self.assertDictEqual(response.data['items'][0], {
            'id': 'TR789ABCD',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'EUR',
            'buy_amount': Decimal('535.55'),
            'rate': Decimal('1.0711'),
            'booked_on': self.t1
        })

    def test_post_item(self):
        with patch('app.api.serializers.generate_unique_id',
                   return_value='TR428YR1O'):  # mock id-generator

            response = self.client.post(
                url=url_for('api_v1__trade_resource'),
                json_data={
                    'sell_currency': 'USD',
                    'sell_amount': Decimal('500.00'),
                    'buy_currency': 'EUR',
                    'buy_amount': Decimal('637.80'),
                    'rate': Decimal('1.2756'),
                    'booked_on': self.t2
                })

        self.assertEqual(response.status_code, 201)
        self.assertDictEqual(response.data, {
            'id': 'TR428YR1O',
            'sell_currency': 'USD',
            'sell_amount': Decimal('500.00'),
            'buy_currency': 'EUR',
            'buy_amount': Decimal('637.80'),
            'rate': Decimal('1.2756'),
            'booked_on': self.t2
        })

    def test_post_item_invalid(self):
        response = self.client.post(
            url=url_for('api_v1__trade_resource'),
            json_data={
                'sell_currency': 'USD',
                'sell_amount': Decimal('500.00'),
                'buy_currency': 'EUR',
                'buy_amount': Decimal('633.13'),
                'rate': Decimal('1.2756'),
                'booked_on': self.t2
            })

        self.assertEqual(response.status_code, 400)
        self.assertEqual(response.data['error'], 'Validation error')

        error_message = (
            'Buy amount should be equal to multiplying '
            'the rate against the sell amount')
        self.assertDictEqual(response.data['details'], {
            'sell_amount': [error_message],
            'buy_amount': [error_message],
            'rate': [error_message]
        })

    def test_post_item_already_exists(self):
        response = self.client.post(
            url=url_for('api_v1__trade_resource'),
            json_data={
                'id': 'TR0123456',
                'sell_currency': 'USD',
                'sell_amount': Decimal('500.00'),
                'buy_currency': 'EUR',
                'buy_amount': Decimal('637.80'),
                'rate': Decimal('1.2756'),
                'booked_on': self.t2
            })

        self.assertEqual(response.status_code, 409)
        self.assertEqual(response.data['error'], 'Resource already exists')
