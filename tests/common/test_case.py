# -*- coding: utf-8 -*-
from __future__ import unicode_literals

try:
    import unittest2 as unittest
except ImportError:
    import unittest

from mock import patch

from tests.common import utils
from app.application import Application


class TestCase(utils.AssertsMixin, unittest.TestCase):
    """
    Basic TestCase class
    """
    app = None
    app_context = None
    client = None

    @classmethod
    def setUpClass(cls):
        cls.app = Application('testing')
        cls.app_context = cls.app.app_context()
        cls.app_context.push()
        cls.client = utils.TestClientWrapper(cls.app.test_client())

    @classmethod
    def tearDownClass(cls):
        cls.app_context.pop()

    def addPatch(self, target, *args, **kwargs):
        patcher = patch(target, *args, **kwargs)
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        return mocked

    def addPatchMultiple(self, targets, *args, **kwargs):
        patcher = utils.patch_multiple(targets, *args, **kwargs)
        mocked = patcher.start()
        self.addCleanup(patcher.stop)
        return mocked
