# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
from collections import namedtuple

import mock

from app.common.utils.json import JSONEncoder, JSONDecoder


__all__ = [
    'patch_multiple',
    'AssertsMixin',
    'TestClientWrapper',
    'Attrs',
]


class MultiPatcher(object):

    def __init__(self):
        super(MultiPatcher, self).__init__()
        self._patchers = []

    def __call__(self, targets, *args, **kwargs):
        self._patchers = [mock.patch(target, *args, **kwargs)
                          for target in targets]
        return self

    def __enter__(self):
        return [patcher.__enter__() for patcher in self._patchers]

    def __exit__(self, *args):
        return all([patcher.__exit__(*args) for patcher in self._patchers])

    def start(self):
        return [patcher.start() for patcher in self._patchers]

    def stop(self):
        return [patcher.stop() for patcher in self._patchers]


patch_multiple = MultiPatcher()


class _AssertRaisesDetailsContext(object):

    def __init__(self, excClass, callableObj=None, *args, **kwargs):
        super(_AssertRaisesDetailsContext, self).__init__()
        try:
            if callableObj:
                callableObj(*args, **kwargs)
        except excClass as e:
            self.exc = e
        else:
            assert False, '{0} not raised'.format(excClass.__name__)

    def __enter__(self):
        return self.exc

    def __exit__(self, exc_type, *args):
        return exc_type is None


class AssertsMixin(object):
    assertRaisesDetails = _AssertRaisesDetailsContext


ResponseTuple = namedtuple('ResponseTuple', ['data', 'status_code'])


class TestClientWrapper(object):
    CONTENT_TYPE_JSON = 'application/json'

    def __init__(self, client, accept_type=None, content_type=None):
        super(TestClientWrapper, self).__init__()
        self._client = client
        self._default_accept_type = accept_type
        self._default_content_type = content_type

    def __call__(self, method_name, url, query_params, headers=None,
                 data=None, json_data=None, accept_type=None,
                 content_type=None):
        headers = headers or {}

        accept_type = accept_type or self._default_accept_type
        if accept_type:
            headers['Accept-Type'] = accept_type

        if json_data is not None:
            content_type = self.CONTENT_TYPE_JSON
        content_type = content_type or self._default_content_type
        if content_type:
            headers['Content-Type'] = content_type

        if json_data is not None and data is None:
            data = json.dumps(json_data, cls=JSONEncoder)

        method = getattr(self._client, method_name)
        response = method(url, query_string=query_params,
                          headers=headers, data=data)

        try:
            result = json.loads(response.data.decode('utf-8'), cls=JSONDecoder)
            return ResponseTuple(result, response.status_code)
        except ValueError:
            return ResponseTuple(response.data, response.status_code)

    def get(self, url, query_params=None, headers=None, accept_type=None):
        return self.__call__('get', url, query_params, headers, accept_type)

    def post(self, url, headers=None, data=None, json_data=None,
             accept_type=None, content_type=None):
        return self.__call__('post', url, None, headers, data, json_data,
                             accept_type, content_type)

    def put(self, url, headers=None, data=None, json_data=None,
            accept_type=None, content_type=None):
        return self.__call__('put', url, None, headers, data, json_data,
                             accept_type, content_type)

    def patch(self, url, headers=None, data=None, json_data=None,
              accept_type=None, content_type=None):
        return self.__call__('patch', url, None, headers, data, json_data,
                             accept_type, content_type)

    def delete(self, url, query_params=None, headers=None, accept_type=None):
        return self.__call__('delete', url, query_params, headers, accept_type)


class Attrs(dict):

    def __init__(self, data, primary_key=None):
        super(Attrs, self).__init__(data)
        self._pk_fields = set(primary_key or [])

    def get_primary_key(self):
        if self._pk_fields.intersection(self) == self._pk_fields:
            result = {}
            for attr_name in self._pk_fields:
                result[attr_name] = self[attr_name]
            return result
        return {}

    def has_primary_key(self):
        return bool(self.get_primary_key())
