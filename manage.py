#!/usr/bin/env python
# -*- coding: utf-8 -*-
"""
This file is used for development:
    running development flask application web-server,
    running tests and checking tests coverage,
    getting python shell, making & running migrations, etc.
"""
from __future__ import unicode_literals
from __future__ import print_function

import os

try:
    import unittest2 as unittest
except ImportError:
    import unittest

import coverage
from flask_script import Manager, Shell, Server
from flask_migrate import Migrate, MigrateCommand


COV = coverage.coverage(
    branch=True,
    include='app/*',
    omit=[
        '*/__init__.py',
        '*/migrations/*'
    ]
)
COV.start()


from wsgi import app


manager = Manager(app)

# Migrations
# more info: python manage.py db --help
migrate = Migrate(
    app, app.db, directory=app.config['SQLALCHEMY_MIGRATIONS_DIRECTORY'])
manager.add_command('db', MigrateCommand)

# Make shell context
manager.add_command('shell', Shell(
    make_context=lambda: dict(app=app)
))

# Run server
manager.add_command('runserver', Server())


# Run tests
@manager.option('-c', '--coverage', dest='with_coverage',
                action='store_const', const=True,
                help='Run tests with coverage.')
@manager.command
def test(with_coverage=False):
    tests = unittest.TestLoader().discover('tests')
    result = unittest.TextTestRunner(verbosity=2).run(tests)
    if result.wasSuccessful():
        if not with_coverage:
            return 0
        base_dir = os.path.abspath(os.path.dirname(__file__))
        cov_dir = os.path.join(base_dir, 'tests', '.reports', 'coverage')
        os.makedirs(cov_dir, exist_ok=True)
        COV.stop()
        COV.save()
        print('\r\nCoverage Summary:')
        COV.report()
        cov_file = os.path.join(cov_dir, 'index.html')
        COV.html_report(directory=cov_dir)
        print('\r\nHTML version: file://{0}\r\n'.format(cov_file))
        COV.erase()
        return 0
    return 1


if __name__ == '__main__':
    manager.run()
