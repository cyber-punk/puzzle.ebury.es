.PHONY: install run test docker-build docker-up docker-connect

DIR := $(shell dirname $(realpath $(lastword $(MAKEFILE_LIST))))
SHELL=/bin/bash

ENV := $(DIR)/env/bin
PYTHON := $(ENV)/python
PIP := $(ENV)/pip
PEP8 := $(ENV)/pep8 --max-line-length=79 --exclude="*/migrations/*"
PYLINT := $(ENV)/pylint

STATUS_ERROR := \033[1;31m*\033[0m Error
STATUS_OK := \033[1;32m*\033[0m OK


install-docker:
	sudo apt-key adv \
		--keyserver hkp://p80.pool.sks-keyservers.net:80 \
	--recv-keys 58118E89F3A912897C070ADBF76221572C52609D ;\
	sudo apt-add-repository \
		'deb https://apt.dockerproject.org/repo ubuntu-xenial main' ;\
	sudo apt-get update ;\
	sudo apt-cache policy docker-engine ;\
	sudo apt-get install -y docker-engine ;\
	sudo usermod -aG docker $USER ;\
	sudo su $USER ;\

install-docker-compose:
	sudo curl -L https://github.com/docker/compose/releases/download/1.11.2/docker-compose-`uname -s`-`uname -m` > /usr/local/bin/docker-compose ;\
	sudo chmod +x /usr/local/bin/docker-compose ;\

install-postgresql:
	sudo apt-get install -y postgresql postgresql-contrib ;\
	sudo -u postgres psql -c "CREATE USER docker WITH SUPERUSER PASSWORD 'docker';" ;\
	sudo -u postgres createdb ebury_development ;\
	sudo -u postgres createdb ebury_testing ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

install-python:
	sudo apt-get install -y --no-install-recommends \
		build-essential \
		libssl-dev \
		python3-dev \
		python3-pip ;\
	sudo pip3 install --upgrade pip ;\
	sudo pip3 install --upgrade virtualenv ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

install-system-packages: install-docker install-docker-compose install-postgresql install-python

install-env:
	rm -rf "$(DIR)/env/" ;\
	virtualenv -p /usr/bin/python3.5 --clear "$(DIR)/env/" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

env-activate:
	. $(ENV)/activate

install-python-libs:
	$(PIP) install -U pip ;\
	$(PIP) install --no-cache-dir --upgrade -r "$(DIR)/requirements.txt" ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

install: install-system-packages install-env env-activate install-python-libs


run:
	$(PYTHON) manage.py runserver


docker-build:
	docker-compose stop ;\
	docker-compose rm --force ;\
	docker volume ls -qf dangling=true | xargs -r docker volume rm ;\
	docker-compose build --no-cache ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;

docker-up:
	docker-compose up -d --no-deps --force-recreate --remove-orphans ;\
	docker-compose ps ;\
	if [ $$? -eq 0 ]; then \
		echo -e "${STATUS_OK}" ;\
	else \
		echo -e "${STATUS_ERROR}" ;\
	fi;


docker-connect:
	docker exec -it web_service /bin/bash


test-unittests:
	@$(PYTHON) manage.py test --coverage ;\
	if [ $$? -eq 0 ]; then \
		echo -e "unittests: ... ${STATUS_OK}" ;\
	else \
		echo -e "unittests: ... ${STATUS_ERROR}" ;\
	fi;

test-pep8:
	@$(PEP8) $(DIR)/app ;\
	if [ $$? -eq 0 ]; then \
		echo -e "pep8: ........ ${STATUS_OK}" ;\
	else \
		echo -e "pep8: ........ ${STATUS_ERROR}" ;\
	fi;

test-pylint:
	@$(PYLINT) $(DIR)/app ;\
	if [ $$? -eq 0 ]; then \
		echo -e "pyint: ....... ${STATUS_OK}" ;\
	else \
		echo -e "pyint: ....... ${STATUS_ERROR}" ;\
	fi;

test: test-unittests test-pep8 test-pylint
