# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.application import Application


app = Application()


if __name__ == '__main__':
    app.run()
