$(function (self) {
    self.dataTable = null;
    self.refresh = function () {
        self.dataTable.ajax.reload();
    };
    self.init = function ($table) {
        $.fn.dataTable.ext.errMode = 'throw';
        self.$table = $table;
        self.dataTable = self.$table.DataTable({
            responsive: true,
            processing: true,
            serverSide: true,
            searching: false,
            ordering: false,
            paging: true,
            pageLength: 10,
            ajax: {
                url: self.$table.attr('data-source'),
                type: 'GET',
                contentType: 'application/json',
                // make query parameters
                data: function (params) {
                    return {
                        limit: params.length,
                        offset: params.start
                    }
                },
                // preprocess response
                dataFilter: function (data) {
                    let result = jQuery.parseJSON(data);
                    result.recordsTotal = result.total;
                    result.recordsFiltered = result.total;
                    result.data = [];
                    jQuery.each(result.items, function (i, item) {
                        let row = [];
                        row.push(item.sell_currency);
                        row.push(item.sell_amount);
                        row.push(item.buy_currency);
                        row.push(item.buy_amount);
                        row.push(item.rate);
                        row.push(item.booked_on);
                        result.data.push(row);
                    });

                    return JSON.stringify(result);
                }
            }
        });
    };
}(window.tradesDataTable = window.tradesDataTable || {}));



$(function (self) {
    self._clearData = function () {
        self.sellCurrency = null;
        self.buyCurrency = null;
        self.sellAmount = null;
        self.buyAmount = null;
        self.rate = null;
    };
    self._clearFields = function () {
        self.$selectBuyCurrency.val('');
        self.$selectBuyCurrency.val('');
        self.$inputSellAmount.val('');
        self.$textBuyAmount.empty();
        self.$textRate.empty();
    };
    self._clearError = function () {
        self.$errorBlock.addClass('hidden');
    };
    self._showError = function (data) {
        self.$textError.text(
            data.error + ':' +
            data.details.toSource());
        self.$errorBlock.removeClass('hidden');
    };
    self._loadCurrencyList = function () {
        self._clearError();

        $.ajax({
            url: self.rateApiEndpoint,
            type: 'GET'
        }).done(function (response) {

            if (response.items.length) {
                let item = response.items[0];

                self.$selectSellCurrency.append(
                    '<option value="' + item.base_currency + '">' +
                        item.base_currency +
                    '</option>');
                self.$selectBuyCurrency.append(
                    '<option value="' + item.base_currency + '">' +
                        item.base_currency +
                    '</option>');
            }

            jQuery.each(response.items, function (i, item) {
                self.$selectSellCurrency.append(
                    '<option value="' + item.rate_currency + '">' +
                        item.rate_currency +
                    '</option>');
                self.$selectBuyCurrency.append(
                    '<option value="' + item.rate_currency + '">' +
                        item.rate_currency +
                    '</option>');
            });

        }).fail(function (response) {
            console.log(response);
            self._showError(response.responseJSON);
        });
    };
    self._loadCurrencyRate = function () {
        self._clearError();

        if (!self.$selectSellCurrency.val() ||
              !self.$selectBuyCurrency.val()) {

            self.rate = null;
            self.$textRate.empty();

            return;
        }

        self.sellCurrency = self.$selectSellCurrency.val();
        self.buyCurrency = self.$selectBuyCurrency.val();

        let url = self.rateApiEndpoint.replace(
            'USD', self.sellCurrency) +
                '/to/' + self.buyCurrency;

        $.ajax({
            url: url,
            type: 'GET'
        }).done(function (response) {

            self.rate = parseFloat(response.rate);
            self.$textRate.text(self.rate);

            self._calculateBuyAmount();

        }).fail(function (response) {

            self.rate = null;
            self.$textRate.empty();

            console.log(response);
            self._showError(response.responseJSON);
        });
    };
    self._calculateBuyAmount = function () {
        if (self.$inputSellAmount.val()) {
            self.sellAmount = parseFloat(
                self.$inputSellAmount.val());
        } else {
            self.sellAmount = 0;
        }
        self.buyAmount = (self.rate * self.sellAmount).toFixed(2);
        self.$textBuyAmount.text(self.buyAmount);
    };
    self._submit = function () {
        self._clearError();

        $.ajax({
            url: self.apiEndpoint,
            type: 'POST',
            contentType: 'application/json',
            dataType: "json",
            data: JSON.stringify({
                sell_currency: self.sellCurrency,
                buy_currency: self.buyCurrency,
                sell_amount: self.sellAmount,
                buy_amount: self.buyAmount,
                rate: self.rate
            })
        }).done(function (response) {

            self.$buttonReset.click();
            tradesDataTable.refresh();

        }).fail(function (response) {
            console.log(response);
            self._showError(response.responseJSON);
        });
    };
    self.init = function ($formBlock, $tradeCreateButton,
                                $tradesTableBlock) {
        self._clearData();

        self.$createButton = $tradeCreateButton;
        self.$formBlock = $formBlock;
        self.$tableBlock = $tradesTableBlock;

        self.$form = self.$formBlock.find('form');
        self.apiEndpoint = self.$form.attr('action');
        self.rateApiEndpoint = self.$form.attr('data-rate-source');
        self.$selectSellCurrency = self.$form.find(
            'select[data-name="sell_currency"]');
        self.$selectBuyCurrency = self.$form.find(
            'select[data-name="buy_currency"]');
        self.$inputSellAmount = self.$form.find(
            'input[data-name="sell_amount"]');
        self.$textBuyAmount = self.$form.find(
            'p[data-name="buy_amount"]');
        self.$textRate = self.$form.find(
            'p[data-name="rate"]');
        self.$buttonSubmit = self.$form.find(
            'button[type="submit"]');
        self.$buttonReset = self.$form.find(
            'button[type="reset"]');
        self.$errorBlock = self.$formBlock.find(
            'div[data-name="error-block"]');
        self.$textError = self.$formBlock.find(
            'p[data-name="error-text"]');
        self.$buttonCloseError = self.$formBlock.find(
            'button[data-name="close-error"]');

        // switch between table & form
        self.$buttonReset.on('click', function () {
            self._clearData();
            self._clearFields();
            self._clearError();
            self.$tableBlock.toggleClass('hidden');
            self.$formBlock.toggleClass('hidden');
        });
        self.$createButton.on('click', function () {
            self.$buttonReset.click();
        });

        self._loadCurrencyList();
        self.$selectBuyCurrency.on('change', function () {
            self._loadCurrencyRate();
        });
        self.$selectBuyCurrency.on('change', function () {
            self._loadCurrencyRate();
        });
        self.$inputSellAmount.on('change', function () {
            self._calculateBuyAmount();
        });
        self.$inputSellAmount.on('keyup', function () {
            self._calculateBuyAmount();
        });
        self.$buttonSubmit.on('click', function (e) {
            self._submit();
            e.preventDefault();
            return false;
        });
        self.$buttonCloseError.on('click', function (e) {
            self._clearError();
            e.preventDefault();
            return false;
        });
    }
}(window.tradesForm = window.tradesForm || {}));


$(document).ready(function() {

    let $table = $('#trades-data-table');
    if ($table.length !== 0) {
        tradesDataTable.init($table);
    }

    let $formBlock = $('#trade-form-block');
    let $tradeCreateButton = $('#trade-create-button');
    let $tradesTableBlock = $('#trades-table-block');
    if ($formBlock.length !== 0 &&
            $tradeCreateButton.length !== 0 &&
            $tradesTableBlock.length !== 0) {
        tradesForm.init($formBlock, $tradeCreateButton, $tradesTableBlock);
    }

});
