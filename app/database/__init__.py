# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys

from flask_sqlalchemy import SQLAlchemy


db = getattr(sys.modules[__name__], 'db', SQLAlchemy())
