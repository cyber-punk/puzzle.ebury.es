# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.default import DefaultConfig


class ProductionConfig(DefaultConfig):
    DEBUG = False
    HOST = '0.0.0.0'
    SQLALCHEMY_DATABASE_HOST = 'postgresql'
