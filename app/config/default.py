# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os
from decimal import Decimal

from app.config import BasicConfig
from app.common.utils.attrs import cached_property


class DefaultConfig(BasicConfig):
    """
    Default configuration. Define all necessary constants here.
    Then you can redefine some default values in the children of this config.
    """
    # pylint: disable=no-self-use

    HOST = '127.0.0.1'
    PORT = 5000

    @cached_property
    def SERVER_NAME(self):
        return '{0}:{1}'.format(self.HOST, self.PORT)

    MAX_PAGING_LIMIT = 1024

    TIME_ZONE = 'UTC'
    DATE_FORMAT = '%Y-%m-%d'
    DATE_TIME_FORMAT = '%Y-%m-%dT%H:%M:%S%z'

    CURRENCY_CODES = [
        'USD', 'EUR',
        'AUD', 'BGN', 'BRL', 'CAD', 'CHF', 'CNY', 'CZK', 'DKK', 'GBP', 'HKD',
        'HRK', 'HUF', 'IDR', 'ILS', 'INR', 'JPY', 'KRW', 'MXN', 'MYR', 'NOK',
        'NZD', 'PHP', 'PLN', 'RON', 'RUB', 'SEK', 'SGD', 'THB', 'TRY', 'ZAR'
    ]
    CURRENCY_RATE_API_ENDPOINT = 'http://api.fixer.io/latest'

    # ignore currencies with 1000 coins like INR
    # most of currencies have 100 coins like USD or EUR
    MIN_AMOUNT = Decimal('0.01')
    MAX_AMOUNT = Decimal('1000000000.00')
    AMOUNT_PRECISION = 10, 2

    MIN_RATE = Decimal('0.01')
    MAX_RATE = Decimal('1000000000.000000')
    RATE_PRECISION = 10, 6

    @cached_property
    def RESTFUL_JSON(self):
        # Flask-RESTFul configuration
        from app.common.utils import json

        return {'cls': json.JSONEncoder}

    SQLALCHEMY_TRACK_MODIFICATIONS = False
    SQLALCHEMY_ECHO = False

    SQLALCHEMY_DATABASE_HOST = '127.0.0.1'
    SQLALCHEMY_DATABASE_PORT = 5432
    SQLALCHEMY_DATABASE_USER = 'docker'
    SQLALCHEMY_DATABASE_PASSWORD = 'docker'
    SQLALCHEMY_DATABASE_DB_NAME = 'ebury'

    @cached_property
    def SQLALCHEMY_DATABASE_URI(self):
        return 'postgresql://{user}:{password}@{host}:{port}/{db}'.format(
            host=self.SQLALCHEMY_DATABASE_HOST,
            port=self.SQLALCHEMY_DATABASE_PORT,
            user=self.SQLALCHEMY_DATABASE_USER,
            password=self.SQLALCHEMY_DATABASE_PASSWORD,
            db=self.SQLALCHEMY_DATABASE_DB_NAME)

    @cached_property
    def SQLALCHEMY_MIGRATIONS_DIRECTORY(self):
        return os.path.join(self.ROOT_PATH, 'database', 'migrations')
