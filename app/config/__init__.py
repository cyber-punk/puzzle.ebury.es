# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sys
import os
import importlib

from flask.globals import LocalProxy, partial, \
    _lookup_app_object as _lookup_app_context_object


class BasicConfig(object):
    ROOT_PATH = os.path.abspath(
        os.path.dirname(os.path.dirname(os.path.dirname(__file__))))

    def __init__(self, app=None):
        """
        Constructor.
        Application can be configured just after config creation.

        :param app: Flask application instance
        """
        super(BasicConfig, self).__init__()
        if app:
            self.init_app(app)

    def init_app(self, app):
        """
        Calls application configuration method.

        :param app: Flask application instance
        """
        app.config.from_object(self)


class ConfigFactory(object):

    def __call__(self, app=None, name=None):
        """
        Fabric method

        :param app: Instance of Flask application, by default is None
        :param name: Configuration name
        :return: Instance of configuration class,
                 by default returns 'default' configuration
        """
        name = name or 'default'
        module = importlib.import_module('.' + name, package='app.config')
        for key in dir(module):
            class_ = getattr(module, key)
            if (isinstance(class_, type) and
                    issubclass(class_, BasicConfig) and
                    class_.__module__ == module.__name__):
                return class_(app)


Settings = getattr(sys.modules[__name__], 'Settings', ConfigFactory())


def _lookup_app_object(name):
    app = LocalProxy(partial(_lookup_app_context_object, 'app'))
    return getattr(app, name)


settings = LocalProxy(partial(_lookup_app_object, 'settings'))
