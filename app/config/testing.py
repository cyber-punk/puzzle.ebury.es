# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.default import DefaultConfig


class TestingConfig(DefaultConfig):
    DEBUG = False
    TESTING = True
    SQLALCHEMY_DATABASE_DB_NAME = 'ebury_testing'
    CURRENCY_RATE_API_ENDPOINT = None
