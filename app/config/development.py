# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config.default import DefaultConfig


class DevelopmentConfig(DefaultConfig):
    DEBUG = True
    SQLALCHEMY_ECHO = True
    SQLALCHEMY_DATABASE_DB_NAME = 'ebury_development'
