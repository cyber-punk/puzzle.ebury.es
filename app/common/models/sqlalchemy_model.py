# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from sqlalchemy.inspection import inspect
from sqlalchemy.ext.declarative import declared_attr

from app.database import db
from app.common.models.base_model import BaseModel


__all__ = ['SqlAlchemyModel']


class SqlAlchemyModel(db.Model, BaseModel):
    __abstract__ = True

    def __init__(self, **attrs):
        db.Model.__init__(self)
        BaseModel.__init__(self, **attrs)

    @declared_attr
    def __tablename__(cls):  # pylint: disable=no-self-argument
        return cls.get_collection_name()

    @classmethod
    def get_primary_key_fields(cls):
        result = [key.name for key in inspect(cls).primary_key]
        return result

    @classmethod
    def has_foreign_keys(cls):
        # pylint: disable=no-member
        result = any([
            isinstance(attr, (db.ForeignKey, db.ForeignKeyConstraint))
            for attr in inspect(cls).mapper.column_attrs
        ])
        return result
