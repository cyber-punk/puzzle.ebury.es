# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import sqlalchemy.types as types
from dateutil.tz import tzutc

from app.common.utils.timezone import get_timezone


__all__ = [
    'ChoiceType',
    'DecimalType',
    'DateTimeType'
]


class ChoiceType(types.TypeDecorator):
    # pylint: disable=abstract-method
    impl = types.Integer

    def __init__(self, choices, **kw):
        self.choices = dict(choices)
        super(ChoiceType, self).__init__(**kw)

    def process_bind_param(self, value, dialect):
        if value is not None:
            return [k for k, v in self.choices.items() if v == value][0]

    def process_result_value(self, value, dialect):
        if value is not None:
            return self.choices[value]


class DecimalType(types.TypeDecorator):
    # pylint: disable=abstract-method
    impl = types.DECIMAL

    def __init__(self, precision, **kw):
        super(DecimalType, self).__init__(
            precision=precision[0] + precision[1],
            scale=precision[1], asdecimal=True, **kw)


class DateTimeType(types.TypeDecorator):
    # pylint: disable=abstract-method
    impl = types.DateTime

    def process_bind_param(self, value, dialect):
        if value is not None:
            return value.astimezone(tzutc()).replace(tzinfo=None)

    def process_result_value(self, value, dialect):
        if value is not None:
            return value.replace(tzinfo=tzutc()).astimezone(get_timezone())
