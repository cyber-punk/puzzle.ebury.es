# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.models.base_model import BaseModel


__all__ = [
    'BaseModel',
]
