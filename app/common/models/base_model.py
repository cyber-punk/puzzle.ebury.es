# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from collections import Generator, OrderedDict

from app.common.utils.strings import plural, decapitalize
from app.common.utils.attrs import get_attributes


__all__ = ['BaseModel']


class BaseModel(object):

    def __init__(self, **attrs):
        super(BaseModel, self).__init__()
        for attr_name, attr_value in attrs.items():
            if attr_name in self.get_attribute_names():
                setattr(self, attr_name, attr_value)

    def to_dict(self):
        result = {}
        for attr_name in self.get_attribute_names():
            attr_value = getattr(self, attr_name, None)
            if attr_value is not None:
                if issubclass(attr_value.__class__, dict):
                    # reduce to dict
                    attr_value = dict(attr_value)
                elif issubclass(
                        attr_value.__class__, (list, tuple, set, Generator)):
                    # reduce to list
                    # pylint: disable=redefined-variable-type
                    attr_value = list(attr_value)
            result[attr_name] = attr_value
        return result

    @classmethod
    def get_attribute_names(cls):
        for attr_name, _ in get_attributes(cls):
            yield attr_name

    @classmethod
    def get_collection_name(cls):
        return plural(decapitalize(cls.__name__))

    @classmethod
    def get_primary_key_fields(cls):
        return []

    @classmethod
    def get_primary_key(cls, **attrs):
        result = OrderedDict()
        for attr_name in cls.get_primary_key_fields():
            if attrs.get(attr_name) is None:
                return None
            result[attr_name] = attrs[attr_name]
        return result

    @property
    def primary_key(self):
        attrs = self.to_dict()
        return self.get_primary_key(**attrs)
