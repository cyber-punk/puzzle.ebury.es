# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from werkzeug.exceptions import HTTPException
from marshmallow import ValidationError
from flask_restful import Api as FlaskApi


__all__ = ['BaseApiRouter']


class BaseApiRouter(FlaskApi):
    """
    Base application API.
    API_RESOURCES should be list of tuples like: [
        (MyResource, ('/my-resources', '/my-resources/<int:id>')),
    ]
    """

    prefix = ''
    api_resources = []

    def __init__(self, app=None, prefix=None, api_resources=None,
                 *args, **kwargs):
        if prefix is not None:
            self.prefix = prefix
        self.endpoint_prefix = self.prefix.replace('/', '_').strip('_')
        if api_resources is not None:
            self.api_resources = api_resources
        super(BaseApiRouter, self).__init__(app, self.prefix, *args, **kwargs)

    def register_resource(self, resource, path):
        resource_endpoint = str('{prefix}__{resourse}'.format(
            prefix=self.endpoint_prefix,
            resourse=resource.get_endpoint()
        ))
        if isinstance(path, (list, tuple)):
            self.add_resource(resource, *path, endpoint=resource_endpoint)
        else:
            self.add_resource(resource, path, endpoint=resource_endpoint)

    def register_resources(self):
        for resource, path in self.api_resources:
            self.register_resource(resource, path)

    def init_app(self, app):
        self.register_resources()
        super(BaseApiRouter, self).init_app(app)

    def handle_error(self, e):
        result = None
        if isinstance(e, HTTPException):
            result = {'error': e.description,
                      'details': e.kwargs or e.args}, e.code
        elif isinstance(e, ValidationError):
            result = {'error': 'Validation error',
                      'details': e.messages}, 400
        if result:
            return self.make_response(result[0], result[1])
        return super(BaseApiRouter, self).handle_error(e)
