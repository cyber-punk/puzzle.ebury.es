# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from datetime import datetime

import pytz

from app.config import settings


__all__ = [
    'get_timezone',
    'now',
]


def get_timezone():
    return pytz.timezone(settings.TIME_ZONE)


def now():
    return get_timezone().localize(datetime.now().replace(microsecond=0))
