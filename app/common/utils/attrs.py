# -*- coding: utf-8 -*-
from __future__ import unicode_literals


__all__ = [
    'get_attributes',
    'cached_property',
]


def get_attributes(obj):
    """
    :param obj: Class on Instance
    :return: generator of meaning pairs of attribute name, attribute value

    Callable attributes will be ignored.
    """
    for attr_name, attr_value in obj.__dict__.items():
        if (not attr_name.startswith('_') and
                not callable(attr_value) and
                not isinstance(attr_value, (
                    classmethod, staticmethod, property))):
            yield attr_name, attr_value


class cached_property(object):
    """
    Cached property decorator.
    It's used instead classic property decorator.

    Example of usages:

        class MyClass(object):
            my_const_a = 1
            my_const_b = 2

            @cached_property
            def my_const_c(self):
                return self.my_const_a + self.my_const_b
    """

    def __init__(self, func):
        self.func = func

    def __get__(self, instance, cls=None):
        result = instance.__dict__[self.func.__name__] = self.func(instance)
        return result
