# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import json
import uuid
import re
from datetime import datetime, date
from decimal import Decimal


__all__ = ['JSONEncoder', 'JSONDecoder']


class JSONEncoder(json.JSONEncoder):

    # pylint: disable=method-hidden
    def default(self, o):
        from app.config import settings

        if isinstance(o, uuid.UUID):
            return str(o)
        if isinstance(o, datetime):
            return o.strftime(settings.DATE_TIME_FORMAT)
        if isinstance(o, date):
            return o.strftime(settings.DATE_FORMAT)
        if isinstance(o, Decimal):
            return str(o)
        return super(JSONEncoder, self).default(o)


class JSONDecoder(json.JSONDecoder):
    re_uuid = re.compile(r'[0-9a-f]{8}(?:-[0-9a-f]{4}){3}-[0-9a-f]{12}', re.I)
    re_decimal = re.compile(r'\-?[0-9]+\.[0-9]+')

    @classmethod
    def _decode_uuid(cls, s):
        s = s.strip('"')
        if cls.re_uuid.match(s):
            return uuid.UUID(s)

    @staticmethod
    def _decode_datetime(s):
        from app.config import settings

        try:
            return datetime.strptime(s.strip('"'), settings.DATE_TIME_FORMAT)
        except ValueError:
            pass

    @staticmethod
    def _decode_date(s):
        from app.config import settings

        try:
            return datetime.strptime(s.strip('"'), settings.DATE_FORMAT).date()
        except ValueError:
            pass

    @classmethod
    def _decode_decimal(cls, s):
        s = s.strip('"')
        if cls.re_decimal.match(s):
            return Decimal(s)

    @classmethod
    def _decode_string(cls, s):
        result = (
            cls._decode_uuid(s) or
            cls._decode_datetime(s) or
            cls._decode_date(s) or
            cls._decode_decimal(s)
        )
        if result is None:
            result = s
        return result

    # pylint: disable=method-hidden
    @classmethod
    def object_hook(cls, obj):
        if isinstance(obj, dict):
            result = dict([(k, cls.object_hook(v)) for k, v in obj.items()])
        elif isinstance(obj, list):
            result = [cls.object_hook(item) for item in obj]
        elif isinstance(obj, str):
            result = cls._decode_string(obj)
        else:
            result = obj
        return result

    def __init__(self, **kwargs):
        super(JSONDecoder, self).__init__(
            object_hook=self.object_hook, **kwargs)
