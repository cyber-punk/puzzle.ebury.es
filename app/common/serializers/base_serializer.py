# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import Schema, post_load, pre_dump, missing


__all__ = ['BaseSerializer', 'AttributesDict']


class AttributesDict(dict):

    def __init__(self, serializer_cls, data):
        super(AttributesDict, self).__init__(data)
        self.serializer_cls = serializer_cls

    def get_primary_key(self):
        return self.serializer_cls.get_primary_key(self)

    def has_primary_key(self):
        return bool(self.get_primary_key())


class BaseSerializer(Schema):

    class Meta(object):
        attrs_dict_cls = AttributesDict
        primary_key = set()
        ordered = True

    def __init__(self, strict=True, **kwargs):
        self._clean_missing_on_dump = True
        self._dumping_data = None
        super(BaseSerializer, self).__init__(strict=strict, **kwargs)

    @post_load(pass_many=False)
    def load_default_values(self, data):
        if self.partial:
            return data
        for attr_name, attr in self.fields.items():
            if attr_name in data or attr.dump_only or attr.default == missing:
                continue
            if callable(attr.default):
                data[attr_name] = attr.default()
            else:
                data[attr_name] = attr.default
        return data

    @pre_dump(pass_many=False)
    def dump_default_values(self, data):
        for attr_name, attr in self.fields.items():
            if attr_name in data or attr.load_only:
                continue
            if callable(attr.default):
                data[attr_name] = attr.default()
            else:
                data[attr_name] = attr.default
        return data

    @classmethod
    def get_primary_key(cls, data):
        if cls.Meta.primary_key.intersection(data) == cls.Meta.primary_key:
            result = {}
            for attr_name in cls.Meta.primary_key:
                result[attr_name] = data[attr_name]
            return result
        return {}

    def load(self, data, many=None, partial=None):
        if many is None:
            many = isinstance(data, list)
        self.partial = partial
        result, errors = super(BaseSerializer, self).load(data, many, partial)
        if not errors:
            if isinstance(result, dict):
                result = self.Meta.attrs_dict_cls(
                    self.__class__, result)
            elif isinstance(result, list):
                result = [
                    self.Meta.attrs_dict_cls(self.__class__, item)
                    for item in result
                ]
        return result, errors

    def dump(self, obj, many=None, update_fields=True, **kwargs):
        if many is None:
            many = isinstance(obj, list)
        result = super(BaseSerializer, self).dump(
            obj, many, update_fields, **kwargs)
        return result
