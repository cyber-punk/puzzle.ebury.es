# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from marshmallow import fields
from marshmallow.validate import Range

from app.config import settings
from app.common.serializers.base_serializer import BaseSerializer, \
    AttributesDict


__all__ = ['PagingSerializer', 'PagingDict']


class PagingDict(AttributesDict):

    @property
    def limit(self):
        return self.get('limit', 0)

    @limit.setter
    def limit(self, value):
        self['limit'] = value

    @property
    def offset(self):
        return self.get('offset', 0)

    @offset.setter
    def offset(self, value):
        self['offset'] = value

    @property
    def total_count(self):
        return self.get('total', 0)

    @total_count.setter
    def total_count(self, value):
        self['total'] = value

    @property
    def items_list(self):
        return self.get('items', [])

    @items_list.setter
    def items_list(self, value):
        self['items'] = value
        self.items_count = len(value) if value else 0

    @property
    def items_count(self):
        return self.get('count', 0)

    @items_count.setter
    def items_count(self, value):
        self['count'] = value


class PagingSerializer(BaseSerializer):
    limit = fields.Integer(
        validate=Range(min=1, max=settings.MAX_PAGING_LIMIT),
        default=settings.MAX_PAGING_LIMIT)
    offset = fields.Integer(default=0, validate=Range(min=0))
    total = fields.Integer(dump_only=True, default=0, validate=Range(min=0))
    count = fields.Integer(dump_only=True, default=0, validate=Range(min=0))
    items = fields.Nested(BaseSerializer, many=True, partial=False,
                          dump_only=True, default=lambda: [])

    class Meta(BaseSerializer.Meta):
        fields = ['limit', 'offset', 'total', 'count', 'items']
        attrs_dict_cls = PagingDict

    def __init__(self, serializer_cls, **kwargs):
        super(PagingSerializer, self).__init__(**kwargs)
        nested = serializer_cls(many=True, partial=False)
        self.declared_fields['items'].nested = nested
        self.fields['items'].nested = nested
