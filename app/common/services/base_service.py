# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.utils.attrs import get_attributes


__all__ = ['BaseService']


class BaseService(object):
    app = None
    _initialized_classes = []

    def __init__(self, app=None):
        if app is not None and not self.is_initialized():
            self.init_app(app)
        self.set_attrs_from_cls()

    @classmethod
    def is_initialized(cls):
        return cls in BaseService._initialized_classes

    @classmethod
    def init_app(cls, app):
        """
        Method will be called once on first class instance creation.
        """
        cls.app = app
        BaseService._initialized_classes.append(cls)

    def set_attrs_from_cls(self):
        for attr_name, attr_value in get_attributes(self.__class__):
            setattr(self, attr_name, attr_value)
