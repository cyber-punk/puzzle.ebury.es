# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.services.base_service import BaseService
from app.common.services.base_storage import BaseStorage


__all__ = [
    'BaseService',
    'BaseStorage',
]
