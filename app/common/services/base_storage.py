# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.services.base_service import BaseService


__all__ = ['BaseStorage']


_no_primary_key_err_msg = '''\
There is not primary key in attributes.

This method should be called when arguments
contain all full primary key part.\
'''


class BaseStorage(BaseService):

    def commit(self):
        pass

    @staticmethod
    def _check_has_primary_key(params):
        if not params.has_primary_key():
            raise RuntimeError(_no_primary_key_err_msg)

    def get_item(self, filter_params):
        raise NotImplementedError()

    def get_list(self, filter_params, paging):
        raise NotImplementedError()

    def get_count(self, filter_params):
        raise NotImplementedError()

    def create_item(self, params):
        raise NotImplementedError()

    def create_list(self, *list_params):
        result = []
        for params in list_params:
            result.append(self.create_item(params))
        return result

    def update_item(self, params):
        raise NotImplementedError()

    def update_list(self, *list_params):
        result = []
        for params in list_params:
            result.append(self.update_item(params))
        return result

    def delete_item(self, filter_params):
        raise NotImplementedError()

    def delete_list(self, filter_params):
        raise NotImplementedError()
