# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from sqlalchemy.exc import SQLAlchemyError, IntegrityError

from app.database import db
from app.common.services.base_storage import BaseStorage
from app.common.models.sqlalchemy_model import SqlAlchemyModel
from app.common.exceptions import DoesNotExist, AlreadyExists


__all__ = ['SQLAlchemyStorage']


class SQLAlchemyStorage(BaseStorage):
    db = None
    model_cls = SqlAlchemyModel

    @classmethod
    def init_app(cls, app):
        cls.db = db
        cls.db.create_all(app=app)
        super(SQLAlchemyStorage, cls).init_app(app)

    def commit(self):
        try:
            self.db.session.commit()
        except SQLAlchemyError as e:
            self.db.session.rollback()
            if isinstance(e, IntegrityError):
                raise AlreadyExists()
            raise e

    def _get_query(self, filter_params):
        attr_names = list(self.model_cls.get_attribute_names())
        params = dict([
            (attr_name, attr_value)
            for attr_name, attr_value in filter_params.items()
            if attr_name in attr_names
        ])
        return self.db.session.query(self.model_cls).filter_by(**params)

    def _get_sorting(self):
        result = [
            self.db.asc(getattr(self.model_cls, attr_name))
            for attr_name in self.model_cls.get_primary_key_fields()
        ]
        return result

    def get_item(self, filter_params):
        self._check_has_primary_key(filter_params)
        item = self._get_query(filter_params).first()
        if not item:
            raise DoesNotExist(**filter_params)
        result = item.to_dict()
        return result

    def get_list(self, filter_params, paging):
        items = self._get_query(filter_params)\
            .order_by(*self._get_sorting())\
            .limit(paging.limit)\
            .offset(paging.offset)\
            .all()
        result = [item.to_dict() for item in items]
        return result

    def get_count(self, filter_params):
        result = self._get_query(filter_params).count()
        return result

    def create_item(self, params):
        # primary key should be already
        # generated in serializer
        self._check_has_primary_key(params)
        item = self.model_cls(**params)
        self.db.session.add(item)
        return item.to_dict()

    def update_item(self, params):
        self._check_has_primary_key(params)
        item = self._get_query(params.get_primary_key()).first()
        if not item:
            raise DoesNotExist(**params.get_primary_key())
        attr_names = item.get_attribute_names()
        for attr_name, attr_value in params.items():
            if attr_name in attr_names:
                setattr(item, attr_name, attr_value)
        self.db.session.add(item)
        return item.to_dict()

    def delete_item(self, filter_params):
        self._check_has_primary_key(filter_params)
        item = self._get_query(filter_params).first()
        if item:
            self.db.session.delete(item)

    def delete_list(self, filter_params):
        if not self.model_cls.has_foreign_keys():
            self._get_query(filter_params).delete()
        else:
            # batch deleting doesn't support foreign keys
            items = self._get_query(filter_params).all()
            for item in items:
                self.db.session.delete(item)
