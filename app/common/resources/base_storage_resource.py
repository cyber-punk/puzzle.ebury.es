# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask import current_app

from app.common.resources.base_resource import BaseResource
from app.common.serializers import BaseSerializer, PagingSerializer
from app.common.services.base_storage import BaseStorage


__all__ = ['BaseStorageResource']


class BaseStorageResource(BaseResource):
    filter_serializer_cls = BaseSerializer
    data_serializer_cls = BaseSerializer
    paging_serializer_cls = PagingSerializer
    storage_cls = BaseStorage
    allowed_methods = ['get', 'post', 'patch', 'delete']

    def __init__(self):
        super(BaseStorageResource, self).__init__()
        self.filter_serializer = self.filter_serializer_cls()
        self.data_serializer = self.data_serializer_cls()
        self.paging_serializer = self.paging_serializer_cls(
            self.data_serializer_cls)
        self.storage = self.storage_cls(current_app)

    def get(self, **kwargs):
        data = self.get_query_attributes(**kwargs)
        filter_params, _ = self.filter_serializer.load(data, partial=True)
        if filter_params.has_primary_key():
            storage_data = self.storage.get_item(filter_params)
            result, _ = self.data_serializer.dump(storage_data)
        else:
            paging, _ = self.paging_serializer.load(data)
            paging.total_count = self.storage.get_count(filter_params)
            if paging.total_count:
                paging.items_list = self.storage.get_list(
                    filter_params, paging)
            result, _ = self.paging_serializer.dump(paging)
        return result, 200

    def post(self, **kwargs):
        data = self.get_body_attributes(**kwargs)
        validated_data, _ = self.data_serializer.load(data, partial=False)
        if isinstance(validated_data, dict):
            storage_data = self.storage.create_item(validated_data)
        else:
            storage_data = self.storage.create_list(*validated_data)
        self.storage.commit()
        result, _ = self.data_serializer.dump(storage_data)
        return result, 201

    def patch(self, **kwargs):
        data = self.get_body_attributes(**kwargs)
        validated_data, _ = self.data_serializer.load(data, partial=True)
        if isinstance(validated_data, dict):
            storage_data = self.storage.update_item(validated_data)
        else:
            storage_data = self.storage.update_list(*validated_data)
        self.storage.commit()
        result, _ = self.data_serializer.dump(storage_data)
        return result, 202

    def delete(self, **kwargs):
        data = self.get_query_attributes(**kwargs)
        filter_params, _ = self.filter_serializer.load(data, partial=True)
        if filter_params.has_primary_key():
            self.storage.delete_item(filter_params)
        else:
            self.storage.delete_list(filter_params)
        self.storage.commit()
        return {}, 204
