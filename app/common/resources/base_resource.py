# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from flask import request
from flask_restful import Resource
from werkzeug.exceptions import MethodNotAllowed

from app.common.utils.strings import decapitalize


__all__ = ['BaseResource']


class BaseResource(Resource):
    allowed_methods = []

    def dispatch_request(self, *args, **kwargs):
        if request.method.lower() not in self.allowed_methods:
            raise MethodNotAllowed(valid_methods=self.allowed_methods)
        return super(BaseResource, self).dispatch_request(*args, **kwargs)

    @classmethod
    def get_endpoint(cls):
        return decapitalize(cls.__name__)

    @classmethod
    def get_path_attributes(cls, **kwargs):
        result = {k: v for k, v in kwargs.items() if v is not None}
        return result

    @classmethod
    def get_headers_attributes(cls):
        result = {}
        for key, value in request.headers.items():
            attr_name = key.lower().replace('-', '_').replace('http_', '', 1)
            attr_value = value.encode('latin1').decode('utf-8')
            result[attr_name] = attr_value
        return result

    @classmethod
    def get_query_attributes(cls, **kwargs):
        result = request.args.to_dict()
        result.update(cls.get_headers_attributes())
        result.update(cls.get_path_attributes(**kwargs))
        return result

    @classmethod
    def get_body_attributes(cls, **kwargs):
        result = request.get_json() or request.form.to_dict()
        headers_attributes = cls.get_headers_attributes()
        path_attributes = cls.get_path_attributes(**kwargs)
        if isinstance(result, list):
            for item in result:
                item.update(headers_attributes)
                item.update(path_attributes)
        else:
            result.update(headers_attributes)
            result.update(path_attributes)
        return result
