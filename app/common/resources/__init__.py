# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.common.resources.base_resource import BaseResource
from app.common.resources.base_storage_resource import BaseStorageResource


__all__ = [
    'BaseResource',
    'BaseStorageResource',
]
