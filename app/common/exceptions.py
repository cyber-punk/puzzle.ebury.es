# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from werkzeug.exceptions import HTTPException


__all__ = [
    'ResourceError',
    'DoesNotExist',
    'AlreadyExists'
]


class ResourceError(HTTPException):
    description = 'Resource error'
    code = 500

    def __init__(self, *args, **kwargs):
        if kwargs.get('code') is not None:
            self.code = kwargs.pop('code')
        if kwargs.get('description') is not None:
            self.description = kwargs.pop('description')
        self.kwargs = kwargs
        HTTPException.__init__(self, *args)


class DoesNotExist(ResourceError):
    description = 'Resource does not exist'
    code = 404


class AlreadyExists(ResourceError):
    description = 'Resource already exists'
    code = 409
