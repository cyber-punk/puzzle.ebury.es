# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import requests

from app.config import settings
from app.api.models import Trade
from app.common.services import BaseStorage
from app.common.services.sqlalchemy_storage import SQLAlchemyStorage
from app.common.utils.json import JSONDecoder
from app.common.exceptions import ResourceError, DoesNotExist


class TradeStorage(SQLAlchemyStorage):
    model_cls = Trade

    def _get_sorting(self):
        return [
            self.db.desc(Trade.booked_on),
            self.db.desc(Trade.id)
        ]


class CurrencyRateServiceError(ResourceError):
    description = 'Currency rate service error'


class CurrencyRateServiceUnavailableError(CurrencyRateServiceError):
    description = 'Currency rate service is temporary unavailable'
    code = 504


class CurrencyRateService(BaseStorage):
    # pylint: disable=abstract-method
    api_endpoint = None

    @classmethod
    def init_app(cls, app):
        cls.api_endpoint = app.config['CURRENCY_RATE_API_ENDPOINT']
        super(CurrencyRateService, cls).init_app(app)

    def _call_api(self, filter_params):
        rate_currency = filter_params.get('rate_currency')
        if not rate_currency:
            rate_currency = ','.join(settings.CURRENCY_CODES)

        try:
            response = requests.get(
                url=self.api_endpoint,
                params={'base': filter_params['base_currency'],
                        'symbols': rate_currency},
                headers={'Accept-Type': 'application/json; charset=utf8'})
        except (ConnectionError, TimeoutError):
            raise CurrencyRateServiceUnavailableError()

        try:
            data = response.json(cls=JSONDecoder)
        except ValueError:
            raise CurrencyRateServiceError()

        if response.status_code != 200:
            raise CurrencyRateServiceError(code=response.status_code, **data)

        rates = [
            (rate_currency, data['rates'][rate_currency])
            for rate_currency in sorted(data['rates'])
        ]

        result = []
        try:
            for rate_currency, rate in rates:
                result.append({
                    'base_currency': data['base'],
                    'rate_currency': rate_currency,
                    'rate': rate
                })
        except IndexError:
            raise CurrencyRateServiceError()

        return result

    def get_item(self, filter_params):
        self._check_has_primary_key(filter_params)
        result = self._call_api(filter_params)
        if not result:
            raise DoesNotExist(**filter_params.get_primary_key())
        return result[0]

    def get_list(self, filter_params, paging):
        result = self._call_api(filter_params)
        result = result[paging.offset:paging.offset + paging.limit]
        return result

    def get_count(self, filter_params):
        result = self._call_api(filter_params)
        result = len(result)
        return result
