# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.config import settings
from app.database import db
from app.common.models.sqlalchemy_model import SqlAlchemyModel
from app.common.models.sqlalchemy_fields import DecimalType, DateTimeType


class Trade(SqlAlchemyModel):
    id = db.Column(db.String(), primary_key=True)
    sell_currency = db.Column(db.String())
    sell_amount = db.Column(DecimalType(settings.AMOUNT_PRECISION))
    buy_currency = db.Column(db.String())
    buy_amount = db.Column(DecimalType(settings.AMOUNT_PRECISION))
    rate = db.Column(DecimalType(settings.RATE_PRECISION))
    booked_on = db.Column(DateTimeType())
