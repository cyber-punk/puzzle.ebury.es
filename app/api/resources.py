# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.serializers import TradeSerializer, CurrencyRateSerializer
from app.api.services import TradeStorage, CurrencyRateService
from app.common.resources import BaseStorageResource


class TradeResource(BaseStorageResource):
    filter_serializer_cls = TradeSerializer
    data_serializer_cls = TradeSerializer
    storage_cls = TradeStorage
    allowed_methods = ['get', 'post']


class CurrencyRateResource(BaseStorageResource):
    filter_serializer_cls = CurrencyRateSerializer
    data_serializer_cls = CurrencyRateSerializer
    storage_cls = CurrencyRateService
    allowed_methods = ['get']
