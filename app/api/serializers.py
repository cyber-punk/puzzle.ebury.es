# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import decimal

from marshmallow import fields, validates_schema
from marshmallow.validate import Regexp, OneOf, Range, ValidationError

from app.config import settings
from app.common.serializers import BaseSerializer
from app.common.utils.strings import generate_unique_id
from app.common.utils import timezone


class TradeSerializer(BaseSerializer):
    id = fields.String(
        validate=Regexp('^TR[0-9A-Z]{7}$'),
        default=lambda: generate_unique_id(prefix='TR', length=7))
    sell_currency = fields.String(
        validate=OneOf(settings.CURRENCY_CODES),
        required=True)
    sell_amount = fields.Decimal(
        places=settings.AMOUNT_PRECISION[1],
        rounding=decimal.ROUND_HALF_UP, as_string=True,
        validate=Range(min=settings.MIN_AMOUNT, max=settings.MAX_AMOUNT),
        required=True)
    buy_currency = fields.String(
        validate=OneOf(settings.CURRENCY_CODES),
        required=True)
    buy_amount = fields.Decimal(
        places=settings.AMOUNT_PRECISION[1],
        rounding=decimal.ROUND_HALF_UP, as_string=True,
        validate=Range(min=settings.MIN_AMOUNT, max=settings.MAX_AMOUNT),
        required=True)
    rate = fields.Decimal(
        places=settings.RATE_PRECISION[1],
        rounding=decimal.ROUND_HALF_UP, as_string=True,
        validate=Range(min=settings.MIN_RATE, max=settings.MAX_RATE),
        required=True)
    booked_on = fields.DateTime(
        format=settings.DATE_TIME_FORMAT,
        default=timezone.now())

    class Meta(BaseSerializer.Meta):
        primary_key = {'id'}

    @validates_schema(pass_many=False)
    def check_buy_amount(self, data):  # pylint: disable=no-self-use
        buy_amount = data.get('buy_amount', 0)
        sell_amount = data.get('sell_amount', 0)
        rate = data.get('rate', 0)
        if buy_amount != round(sell_amount * rate, 2):
            raise ValidationError(
                'Buy amount should be equal to multiplying '
                'the rate against the sell amount',
                field_names=['buy_amount', 'sell_amount', 'rate'],
                data=data)

        return data


class CurrencyRateSerializer(BaseSerializer):
    base_currency = fields.String(
        validate=OneOf(settings.CURRENCY_CODES),
        required=True)
    rate_currency = fields.String(
        validate=OneOf(settings.CURRENCY_CODES),
        required=True)
    rate = fields.Decimal(
        dump_only=True,
        places=settings.RATE_PRECISION[1],
        rounding=decimal.ROUND_HALF_UP, as_string=True,
        validate=Range(min=settings.MIN_RATE, max=settings.MAX_RATE))

    class Meta(BaseSerializer.Meta):
        primary_key = {'base_currency', 'rate_currency'}
