# -*- coding: utf-8 -*-
from __future__ import unicode_literals

from app.api.resources import TradeResource, CurrencyRateResource
from app.common.router import BaseApiRouter


class ApiRouter(BaseApiRouter):
    prefix = '/api/v1'
    api_resources = [
        (TradeResource, ('/trades', '/trades/<string:id>')),
        # how many <rate_currency> does 1 <base_currency> cost?
        (CurrencyRateResource, (
            '/rates/<string:base_currency>',
            '/rates/<string:base_currency>/to/<string:rate_currency>')),
    ]
