# -*- coding: utf-8 -*-
from __future__ import unicode_literals

import os

from flask import Flask, render_template
from flask.config import ConfigAttribute
from flask_cors import CORS

from app.config import Settings
from app.database import db


class Application(Flask):
    host = ConfigAttribute('HOST')
    port = ConfigAttribute('PORT')

    def __init__(self, config_name=None):
        config_name = config_name or os.environ.get('CONFIG', 'development')
        super(Application, self).__init__(__name__)
        self.settings = Settings(app=self, name=config_name)
        self.db = db
        self.db.init_app(app=self)
        self.api = self.register_api()
        self.register_views(app=self)
        self.cross_origin = CORS(app=self, resources={'/*': {'origins': '*'}})

    def register_api(self):
        with self.app_context():
            from app.api.router import ApiRouter

            return ApiRouter(app=self)

    @staticmethod
    def register_views(app):
        # pylint: disable=unused-variable

        @app.route('/')
        def index_page():
            return render_template('index.html')

    def run(self, host=None, port=None, **options):
        host = host or self.host
        port = port or self.port
        super(Application, self).run(host, port, **options)
